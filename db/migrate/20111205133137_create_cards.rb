class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :vendor
      t.string :code
      t.decimal :amount
      t.integer :user_id

      t.timestamps
    end
  end
end

