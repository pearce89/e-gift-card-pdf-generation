class Card < ActiveRecord::Base

	belongs_to :user

	validates :vendor, :code, :amount, :user_id, :presence => true
	validates :code, :length => {:minimum => 16, :maximum => 16}

end

