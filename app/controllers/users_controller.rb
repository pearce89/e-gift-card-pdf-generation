class UsersController < ApplicationController
	before_filter :authenticate_user!

	def show
		@card = User.find(params[:id]).cards.all
	end

end

