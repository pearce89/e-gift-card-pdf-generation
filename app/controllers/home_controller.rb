class HomeController < ApplicationController
	def index
		if user_signed_in?
			@cards = current_user.cards.all
		end
	end
end

