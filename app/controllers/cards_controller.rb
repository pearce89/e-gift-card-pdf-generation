class CardsController < ApplicationController

	before_filter :authenticate_user!

	def index
	end

	def new
		@user = current_user
		@card = Card.new(:user_id => @user)
	end

	def create
		@card = Card.new(params[:card])

		if @card.save
			flash[:success] = '<strong>Card</strong> was successfully created.'.html_safe
			redirect_to root_path
		else
			if @card.errors.any?
				flashmessage = String.new
				@card.errors.each do |attribute, msg|
					flashmessage += '<strong>' + attribute.to_s + '</strong> ' + msg + '; '
				end
				flash[:error] = flashmessage.html_safe
			end
			redirect_to new_user_card_path
		end
	end

	def show
		@card = Card.find(params[:id])
		respond_to do |format|
			format.html
			format.pdf do
				render :pdf => "card", # pdf will download as my_pdf.pdf
					:layout => 'pdf', # uses views/layouts/pdf.haml
					:show_as_html => params[:debug].present? # renders html version if you set debug=true in URL
			end
		end
	end

	def destroy
	end

	def pdf
		format.pdf do
			render :pdf => "file_name"
		end
	end

end

